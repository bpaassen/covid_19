import csv

dates = []
X = []

columns = ['new_cases', 'positive_rate', 'new_deaths', 'icu_patients', 'weekly_hosp_admissions']
weekly  = [False, False, False, True, True]
ends_early = [False, False, True, True, True]

with open('owid-covid-data.csv') as f:
    reader = csv.reader(f, delimiter = ',')
    header = reader.__next__()
    location_col = header.index('location')
    date_col     = header.index('date')
    cols = []
    for col_name in columns:
        cols.append(header.index(col_name))
    for row in reader:
        if row[location_col] != 'Germany':
            continue
        data_row = []
        for col in cols:
            if row[col] == '':
                data_row.append(0.)
                continue
            data_row.append(float(row[col]))
        dates.append(row[date_col])
        X.append(data_row)

import numpy as np
from scipy.stats import linregress
import matplotlib.pyplot as plt

X = np.array(X)

# perform 7-day-smoothing
Xsmooth = np.cumsum(X, 0)
Xsmooth = (Xsmooth[7:, :] - Xsmooth[:-7, :])/7

# np.savetxt('pandemic_indicators.csv', Xsmooth, fmt = '%g', delimiter = '\t', header = '\t'.join(columns), comments = '')

#Xnorm = Xsmooth / np.expand_dims(np.max(Xsmooth, 0), 0)

#plt.figure(figsize = (16, 5))
#plt.plot(Xnorm)
#plt.xlabel('days since %s' % dates[0])
#plt.ylabel('fraction of maximum value')
#plt.title('various pandemic-relevant measures over time (7-day moving average)')
#plt.legend(columns)
#plt.show()

# set up start and end dates for each exponential fit
start_dates = ['2021-10-13', '2021-10-13', '2021-10-27', '2021-10-27', '2021-10-20']
end_dates   = ['2021-11-16', '2021-11-10', '2021-11-16', '2021-11-05', '2021-11-10']

# perform an exponential fit for every column
plt.figure(figsize = (16, 5))
header  = []
datamat = []
for j in range(len(columns)-1):
    start = dates.index(start_dates[j])
    end   = dates.index(end_dates[j])
    y     = Xsmooth[start-7:end-7, j]
    x     = np.arange(len(y))
    a, b, r, p, _ = linregress(x, np.log(y))
    print('--- %s (from %s) ---' % (columns[j], start_dates[j]))
    print('exponential exp(%g*x+%g) fit achieves r = %g, p = %g' % (a, b, r, p))
    print('estimated doubling time: %g days' % (np.log(2) / a))
    ypred = np.exp(a * x + b)
    plt.subplot(1, len(columns)-1, j+1)
    plt.semilogy(x, y)
    plt.semilogy(x, ypred)
    plt.xlabel('day since %s' % start_dates[j])
    plt.ylabel(columns[j])
    plt.legend(['actual', 'exponential fit'])
    header.append(columns[j])
    header.append(columns[j] + '_pred')
    datamat.append(y)
    datamat.append(ypred)
plt.show()

m = np.max([len(y) for y in datamat])

datamat2 = np.full((m, len(datamat)), np.nan)
for j in range(len(datamat)):
    y = datamat[j]
    datamat2[:len(y), j] = y

np.savetxt('fit_data.csv', datamat2, fmt = '%g', delimiter = '\t', header = '\t'.join(header), comments = '')
