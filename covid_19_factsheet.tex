\documentclass[12pt]{article}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{textcomp}
\usepackage[ngerman]{babel}

\usepackage{tikz}
\usetikzlibrary{automata,arrows,patterns}
\usepackage{tango-colors}

\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\usepgfplotslibrary{groupplots}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{bm}

\usepackage[a4paper, left=3cm, right=2.5cm, top=3cm, bottom=3cm]{geometry}

\begin{document}

\section*{Infoblatt: Indikatoren und exponentielles Wachstum}

\vspace{-0.3cm}
\textbf{Version: 17.11.2021}

\vspace{0.3cm}
Ziel dieses Infoblattes ist es, zwei Kernfragen der Covid-19-Pandemie zu beantworten:
Was sind die wesentlichen Indikatoren, um den Verlauf der Pandemie zu beschreiben?
Und: Was ist eigentlich exponentielles Wachstum?

\subsection*{Indikatoren}

In den Medien gibt es vielfach Streit darüber, ob die Inzidenz, die Rate von Hospitalisierungen,
die Todeszahl, oder andere Indikatoren herangezogen werden sollten, um die Pandemie zu beschreiben
und den Anlass für Maßnahmen zu bestimmen. Aus meiner Sicht werden viele dieser Diskussionen müßig,
wenn man sich einmal den Verlauf der Indikatoren anschaut. Abbildung~\ref{fig:indikatoren} zeigt
den Verlauf von neuen Fällen, positiver Testrate, Hospitalisierungen, Intensivpatient*innen und
Toten (je gemittelt über sieben Tage).

Wie deutlich zu erkennen ist hängen sämtliche Indikatoren zusammen. Die neuen Fälle steigen 
üblicherweise parallel zur Positiv-Testrate. Die Krankenhauseinweisungen steigen mit einigen
Tagen Verzögerung, die Zahl der Intensivpatient*innen mit einigen Wochen Verzögerung und die
Todeszahlen mit etwa einer weiteren Woche Verzögerung.

Dabei sind die Zahlenverhältnisse zwischen den Indikatoren nicht immer gleich.
Zum Beispiel: In der derzeitigen Welle übersetzen sich neue Fälle in weniger Krankenhauseinweisungen,
Intensivpatient*innen und Tote als in den vorigen Wellen---ein Fakt, der höchstwahrscheinlich
auf Impfungen zurückzuführen ist. Trotzdem steigen alle Indikatoren immer noch in Folge der Fälle.
Daher sind die Fälle (bzw.\ die Inzidenz oder die Positiv-Testrate) noch immer der beste
Frühwarnindikator, den wir haben. Und daher gilt es immer noch, vor allem Fälle, Inzidenz und 
Positiv-Testrate als Kriterium für Maßnahmen heranzuziehen---auch, wenn die exakte Höhe anders
zu bewerten ist als früher.

\begin{figure}[!b]
\begin{center}
\begin{tikzpicture}
\begin{axis}[width=16cm, height=5.5cm, xlabel={Tage seit 27.01.2020}, xmin=0, xmax=660,
scaled ticks=false, ymin=0, 
legend pos={north west}, legend cell align=left, legend style={draw=none,fill=none}]
\addplot[thick, skyblue3] table[x expr=\coordindex,y=new_cases,col sep=tab] {pandemic_indicators.csv};
\addlegendentry{Neue Fälle}
\addplot[thick, chameleon3] table[x expr=\coordindex,y expr={\thisrow{positive_rate}*1000000},col sep=tab] {pandemic_indicators.csv};
\addlegendentry{Positiv-Testrate x $10^6$}
\addplot[thick, butter3] table[x expr=\coordindex,y expr={\thisrow{weekly_hosp_admissions}*10},col sep=tab] {pandemic_indicators.csv};
\addlegendentry{Krankenhauseinweisungen x 10}
\addplot[thick, orange3] table[x expr=\coordindex,y expr={\thisrow{icu_patients}},col sep=tab] {pandemic_indicators.csv};
\addlegendentry{Intensivpatient*innen}
\addplot[thick, scarletred3] table[x expr=\coordindex,y expr={\thisrow{new_deaths} * 10},col sep=tab] {pandemic_indicators.csv};
\addlegendentry{Todesfälle x 10}
\end{axis}
\end{tikzpicture}
\end{center}
\caption{Der Zeitverlauf mehrerer pandemischer Indikatoren in Deutschland (gemittelt über sieben Tage; Datenquelle: \url{https://ourworldindata.org/explorers/coronavirus-data-explorer}).}
\label{fig:indikatoren}
\end{figure}

\clearpage

\subsection*{Exponentielles Wachstum}

Exponentielles Wachstum besteht, wenn eine Ausgabegröße $y$ sich vervielfacht, obwohl sich die
Eingabegröße nur um einen konstanten Wert vergrößert. Angewandt auf die Covid-19-Pandemie heißt das:
Wenn wir eine bestimmte Zeit länger warten verdoppelt sich ein Indikator.

Abbildung~\ref{fig:exponential_fits} zeigt die Entwicklung einiger Indikatoren (blau) für die letzten
Wochen und legt darüber (in gestrichelt, orange) eine passende Exponentialfunktion.
Die orangene Zahl ist die jeweilige Verdopplungszeit. Zum Beispiel hat sich die Anzahl neuer Fälle 
seit dem 13.10.2021 im wesentlichen alle 14,5 Tage verdoppelt, die Test-Positivrate etwa alle drei 
Wochen, die Zahl der Intensivpatient*innen würde sich aktuell etwa alle 23 Tage verdoppeln
und die Todesfälle etwa alle 16,2 Tage.
Der Vergleich aus Exponentialfunktion (orange) und tatsächlichen Werten (blau) legt nahe: Die
Übereinstimmung ist nicht zufällig. Das offenkundige Wachstumsverhalten ist exponentiell.
Wenn dieses Wachstum weiter geht werden in 16 Tagen etwa 300 Menschen pro Tag sterben und in einem
Monat etwa 600 Menschen pro Tag. Wann das exponentielle Wachstum aufhört ist sehr schwer vorherzusagen
und hängt von unserem Verhalten ab: Wie viele Kontakte wir pflegen, ob wir Masken dabei tragen,
ob sich mehr Menschen impfen lassen und so weiter.

Selbst wenn wir unser Verhalten strikt ändern gibt es aber einen Zeitverzug. Infektionen, die bereits
geschehen sind lassen sich nicht mehr verhindern. Deshalb werden so oder so die 
Krankenhauseinweisungen, Intensivpatient*innen und Todesfälle noch mehrere Woche steigen.
Auch deshalb ist es so wichtig, Fallzahlen (bzw.\ Inzidenz oder Test-Positivrate) als 
Frühwarnindikatoren zu nutzen: Um dem Zeitverzug zuvorzukommen.

\paragraph{Autor:} Dr.\ Benjamin Paaßen ist Wissenschaftler für künstliche Intelligenz und
\emph{kein} Experte für Pandemien. Allerdings bekommt
er es grad noch hin, Daten herunterzuladen und zu plotten. Mehr war für dieses Dokument nicht
nötig.

\begin{figure}[!b]
\begin{center}
\begin{tikzpicture}
\begin{axis}[width=4cm, height=4.5cm, xlabel={Tage seit 13.10.2021}, xmin=0, xmax=35,
scaled ticks=false, ymin=0, title={$\strut$Neue Fälle}]
\addplot[thick, skyblue3] table[x expr=\coordindex,y=new_cases,col sep=tab] {fit_data.csv};
\addplot[thick, orange3, densely dashed, domain=0:33, samples=101] {exp(0.0479001*x+8.99869)};
\node[orange3, above] at (axis cs:15,30000) {14,5 Tage};
\end{axis}
%
\begin{scope}[shift={(4,0)}]
\begin{axis}[width=4cm, height=4.5cm, xlabel={Tage seit 13.10.2021}, xmin=0, xmax=28,
scaled ticks=false, ymin=0, title={$\strut$Test-Positivrate},
y tick label style={
	/pgf/number format/.cd,
	fixed,
	fixed zerofill,
	precision=2,
	/tikz/.cd
},]
\addplot[thick, skyblue3] table[x expr=\coordindex,y=positive_rate,col sep=tab] {fit_data.csv};
\addplot[thick, orange3, densely dashed, domain=0:28, samples=101] {exp(0.0318553*x+-4.67321)};
\node[orange3, above] at (axis cs:11,0.019) {21,8 Tage};
\end{axis}
\end{scope}
%
\begin{scope}[shift={(8,0)}]
\begin{axis}[width=4cm, height=4.5cm, xlabel={Tage seit 27.10.2021}, xmin=0, xmax=9,
scaled ticks=false, ymin=0, title={$\strut$Intensivpatient*innen}]
\addplot[thick, skyblue3] table[x expr=\coordindex,y=icu_patients,col sep=tab] {fit_data.csv};
\addplot[thick, orange3, densely dashed, domain=0:9, samples=101] {exp(0.0300761*x+7.38756)};
\node[orange3, above] at (axis cs:4,1800) {23 Tage};
\end{axis}
\end{scope}
%
\begin{scope}[shift={(12,0)}]
\begin{axis}[width=4cm, height=4.5cm, xlabel={Tage seit 27.10.2021}, xmin=0, xmax=21,
scaled ticks=false, ymin=0, title={$\strut$Todesfälle}]
\addplot[thick, skyblue3] table[x expr=\coordindex,y=new_deaths,col sep=tab] {fit_data.csv};
\addplot[thick, orange3, densely dashed, domain=0:20, samples=101] {exp(0.0428064*x+4.3535)};
\node[orange3, above] at (axis cs:9,150) {16,2 Tage};
\end{axis}
\end{scope}
\end{tikzpicture}
\end{center}
\caption{Der kürzliche Zeitverlauf mehrerer pandemischer Indikatoren in Deutschland (gemittelt über 
sieben Tage; Datenquelle: \url{https://ourworldindata.org/explorers/coronavirus-data-explorer})
und eine exponentielle Funktion daneben. Die Zahlen geben die Verdopplungszeit an.}
\label{fig:exponential_fits}
\end{figure}

\end{document}