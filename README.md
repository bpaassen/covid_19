# covid_19

A small Python3 script to visualize the development of Covid-19 over time for different countries.

Usage

```
python3 cov visualize_covid_19.py -c "Italy,France,Germany,Republic of Korea"
```
