#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generates two plots with a line for every input country, namely:

* A log plot of the overall number of confirmed cases
* A plot of the growth rate (new cases[t+1] / new cases[t])
"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2020 Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import argparse
import csv
import numpy as np
import urllib.request
import matplotlib.pyplot as plt
try:
    import seaborn as sns
    sns.set()
except Exception as ex:
    pass

_CSV_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
_COUNTRY_COL = 1
_TIME_SERIES_COL = 4

# parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="A path to the time_series_19-covid-Confirmed.csv file from John Hopkins CSSE. If not given, the data is pulled from the web.")
parser.add_argument("-c", "--countries", help="A comma-separated list of countries which should be visualized. The country names must be consistent with the time_series_19-covid-Confirmed.csv file from John Hopkins CSSE. Mainland China,Republic of Korea,Italy,Germany per default.")
args = parser.parse_args()

# read set of countries
if args.countries:
    countries = args.countries.split(',')
else:
    countries = ['France', 'Korea, South', 'Italy', 'Germany']

# import the data as string, either from file or from the web
if args.input:
    with open(args.input) as f:
        csv_data = f.read().split('\n')
else:
    http_request = urllib.request.urlopen(_CSV_URL)
    csv_data = http_request.read().decode('utf-8').split('\n')

# initialize a dictionary of countries to time series
data = {}

# parse the CSV data, extracting only the lines that we need
csv_reader = csv.reader(csv_data, delimiter=',', quotechar='"')
# get the dates from the first row
dates = csv_reader.__next__()[_TIME_SERIES_COL:]
T = len(dates)
for row in csv_reader:
    if len(row) < 1:
        continue
    # compare country column to set of countries; if it's not in there,
    # we ignore the current row
    country = row[_COUNTRY_COL]
    if country not in countries:
        continue
    # then add the time series to the data for the respective country
    time_series = data.setdefault(country, np.zeros(T, dtype=int))
    time_series += np.array(row[_TIME_SERIES_COL:], dtype=int)

# check if all countries were found
for country in countries:
    if country not in data:
        raise ValueError('Country %s was not found in the data.' % country)

# visualize the data in a logarithmic plot along with the best linear fit
for country in countries:
    plt.semilogy(data[country])
plt.xticks(list(range(T)), dates, rotation = 45, ha="right")
plt.legend(countries)
plt.xlabel('time')
plt.ylabel('confirmed cases')
plt.title('Confirmed cases [log]')
plt.show()

# do a growth rate plot
plt.semilogy([0, T-2], [1., 1.], 'k.-')
for country in countries:
    time_series  = data[country]
    new_cases    = time_series[1:] - time_series[:-1]
    growth_rates = np.zeros(T-2)
    curr = new_cases[1:]
    prev = new_cases[:-1]
    growth_rates[prev > 1E-3] = curr[prev > 1E-3] / prev[prev > 1E-3]
    growth_rates[prev <= 1E-3] = 1.
    growth_rates[curr <= 1E-3] = 1.
    plt.semilogy(growth_rates)
plt.xticks(list(range(T-2)), dates[:-2], rotation = 45, ha="right")
plt.legend(['baseline'] + list(countries))
plt.xlabel('time')
plt.ylabel('growth rate')
plt.title('Growth rate [log]')
plt.show()
